﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnSumar_Click(object sender, EventArgs e)
        {
            int n1, n2, suma;

            n1 = int.Parse(txtNumero1.Text.Trim());

            n2 = int.Parse(txtNumero2.Text.Trim());

            suma = n1 + n2;

            txtResultado.Text = suma.ToString();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNumero1.Clear();
            txtNumero2.Text = "";
            txtResultado.ResetText();



        }
    }
}
