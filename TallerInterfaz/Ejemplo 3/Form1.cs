﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int total = 0;

            if (rbn1.Checked == true)
            {
                total = total + 50;
            }
            if (rbn2.Checked == true)
            {
                total = total + 100;
            }
            if (rbn3.Checked == true)
            {
                total = total + 150;
            }
            if (this.chx1.Checked == true)
            {
                total = total + 5;
            }
            if (this.chx2.Checked == true)
            {
                total = total + 15;
            }
            if (this.chx3.Checked == true)
            {
                total = total + 20;
            }
            MessageBox.Show("El total de gastos de envios es: " + total.ToString("c2"));


        }
    }
}
