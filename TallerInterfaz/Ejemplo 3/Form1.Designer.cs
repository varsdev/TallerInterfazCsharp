﻿namespace Ejemplo_3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbn1 = new System.Windows.Forms.RadioButton();
            this.rbn2 = new System.Windows.Forms.RadioButton();
            this.rbn3 = new System.Windows.Forms.RadioButton();
            this.chx1 = new System.Windows.Forms.CheckBox();
            this.chx2 = new System.Windows.Forms.CheckBox();
            this.chx3 = new System.Windows.Forms.CheckBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Como desea recibir el pedido? (Marcar solo una)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(504, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Como desea ser notificado del envio? (Marque todas las que apliquen)";
            // 
            // rbn1
            // 
            this.rbn1.AutoSize = true;
            this.rbn1.Checked = true;
            this.rbn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbn1.Location = new System.Drawing.Point(47, 77);
            this.rbn1.Name = "rbn1";
            this.rbn1.Size = new System.Drawing.Size(193, 24);
            this.rbn1.TabIndex = 2;
            this.rbn1.TabStop = true;
            this.rbn1.Text = "Por correo normal (50$)";
            this.rbn1.UseVisualStyleBackColor = true;
            // 
            // rbn2
            // 
            this.rbn2.AutoSize = true;
            this.rbn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbn2.Location = new System.Drawing.Point(47, 118);
            this.rbn2.Name = "rbn2";
            this.rbn2.Size = new System.Drawing.Size(233, 24);
            this.rbn2.TabIndex = 3;
            this.rbn2.Text = "Por paqueteria normal (100$)";
            this.rbn2.UseVisualStyleBackColor = true;
            // 
            // rbn3
            // 
            this.rbn3.AutoSize = true;
            this.rbn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbn3.Location = new System.Drawing.Point(47, 161);
            this.rbn3.Name = "rbn3";
            this.rbn3.Size = new System.Drawing.Size(240, 24);
            this.rbn3.TabIndex = 4;
            this.rbn3.Text = "Por paqueteria urgente (150$)";
            this.rbn3.UseVisualStyleBackColor = true;
            // 
            // chx1
            // 
            this.chx1.AutoSize = true;
            this.chx1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chx1.Location = new System.Drawing.Point(47, 261);
            this.chx1.Name = "chx1";
            this.chx1.Size = new System.Drawing.Size(125, 24);
            this.chx1.TabIndex = 5;
            this.chx1.Text = "Por email (5$)";
            this.chx1.UseVisualStyleBackColor = true;
            // 
            // chx2
            // 
            this.chx2.AutoSize = true;
            this.chx2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chx2.Location = new System.Drawing.Point(47, 305);
            this.chx2.Name = "chx2";
            this.chx2.Size = new System.Drawing.Size(155, 24);
            this.chx2.TabIndex = 6;
            this.chx2.Text = "Por telefono (15$)";
            this.chx2.UseVisualStyleBackColor = true;
            // 
            // chx3
            // 
            this.chx3.AutoSize = true;
            this.chx3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chx3.Location = new System.Drawing.Point(47, 349);
            this.chx3.Name = "chx3";
            this.chx3.Size = new System.Drawing.Size(118, 24);
            this.chx3.TabIndex = 7;
            this.chx3.Text = "Por fax (20$)";
            this.chx3.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.Location = new System.Drawing.Point(126, 400);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(336, 46);
            this.btnCalcular.TabIndex = 8;
            this.btnCalcular.Text = "Calcular gastos de envio";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 458);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.chx3);
            this.Controls.Add(this.chx2);
            this.Controls.Add(this.chx1);
            this.Controls.Add(this.rbn3);
            this.Controls.Add(this.rbn2);
            this.Controls.Add(this.rbn1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Pantalla de detalles de envio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbn1;
        private System.Windows.Forms.RadioButton rbn2;
        private System.Windows.Forms.RadioButton rbn3;
        private System.Windows.Forms.CheckBox chx1;
        private System.Windows.Forms.CheckBox chx2;
        private System.Windows.Forms.CheckBox chx3;
        private System.Windows.Forms.Button btnCalcular;
    }
}

