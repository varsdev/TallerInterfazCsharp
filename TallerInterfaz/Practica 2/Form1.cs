﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double cel, far;

            cel = double.Parse(txtCentigrados.Text.Trim());
            far = double.Parse(txtFarenheit.Text.Trim());

            if (cel != 0)
            {
                txtFarenheit.Text = ((cel * 1.8) + 32).ToString();
            }
            if (far != 0)
            {
                txtCentigrados.Text = ((far - 32) / 1.8).ToString();
            }
            btnConvertir.Enabled = false;

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtCentigrados.Text = "0,00";
            txtFarenheit.Text = "0,00";
            btnConvertir.Enabled = true;
        }
    }
}
