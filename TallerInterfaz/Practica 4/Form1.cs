﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int arabigo;
            arabigo = int.Parse(txtN.Text.Trim());
            if (arabigo > 3999)
            {
                MessageBox.Show("Numero fuera de rango");
            }
            else
            {
                txtR.Text = romanizar(arabigo);
            }
        }

        string romanizar(int arabigos)
        {
            string romano = "";
            while (arabigos >= 1000)
            {
                arabigos = arabigos - 1000;
                romano = romano + "M";
            }
            if (arabigos >= 900)
            {
                arabigos = arabigos - 900;
                romano = romano + "CM";
            }
            if (arabigos >= 500)
            {
                arabigos = arabigos - 500;
                romano = romano + "D";
            }
            if (arabigos >= 400)
            {
                arabigos = arabigos - 400;
                romano = romano + "CD";
            }
            if (arabigos >= 100)
            {
                arabigos = arabigos - 100;
                romano = romano + "C";
            }
            if (arabigos >= 90)
            {
                arabigos = arabigos - 90;
                romano = romano + "XC";
            }
            if (arabigos >= 50)
            {
                arabigos = arabigos - 50;
                romano = romano + "L";
            }
            if (arabigos >= 40)
            {
                arabigos = arabigos - 40;
                romano = romano + "XL";
            }
            if (arabigos >= 10)
            {
                arabigos = arabigos - 10;
                romano = romano + "X";
            }
            if (arabigos >= 9)
            {
                arabigos = arabigos - 9;
                romano = romano + "IX";
            }
            if (arabigos >= 5)
            {
                arabigos = arabigos - 5;
                romano = romano + "V";
            }
            if (arabigos >= 4)
            {
                arabigos = arabigos - 4;
                romano = romano + "IX";
            }
            if (arabigos >= 1)
            {
                arabigos = arabigos - 1;
                romano = romano + "I";
            }

            return romano;

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtN.ResetText();
            txtR.ResetText();
        }
    }
}
