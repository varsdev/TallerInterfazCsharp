﻿namespace Practica_3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.cbxFueCol = new System.Windows.Forms.ComboBox();
            this.btnDesaparecio = new System.Windows.Forms.Button();
            this.btnAparecio = new System.Windows.Forms.Button();
            this.btnLimpiarTxt = new System.Windows.Forms.Button();
            this.btnEnviarTxt = new System.Windows.Forms.Button();
            this.btnCamColLet = new System.Windows.Forms.Button();
            this.btnDesbloquear = new System.Windows.Forms.Button();
            this.btnBloquear = new System.Windows.Forms.Button();
            this.btnCamColFon = new System.Windows.Forms.Button();
            this.txtTexto = new System.Windows.Forms.TextBox();
            this.txtPanel = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(442, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "MODIFICANDO PROPIEDADES DE CONTROL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Visibilidad:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(330, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Accesibilidad:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(330, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Texto:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Color de fuente y fondo:";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(27, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(0, 0);
            this.panel1.TabIndex = 5;
            // 
            // cbxFueCol
            // 
            this.cbxFueCol.FormattingEnabled = true;
            this.cbxFueCol.Items.AddRange(new object[] {
            "Azul",
            "Verde",
            "Rojo",
            "Naranja",
            "Negro",
            "Blanco"});
            this.cbxFueCol.Location = new System.Drawing.Point(28, 280);
            this.cbxFueCol.Name = "cbxFueCol";
            this.cbxFueCol.Size = new System.Drawing.Size(171, 21);
            this.cbxFueCol.TabIndex = 7;
            // 
            // btnDesaparecio
            // 
            this.btnDesaparecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesaparecio.Location = new System.Drawing.Point(27, 163);
            this.btnDesaparecio.Name = "btnDesaparecio";
            this.btnDesaparecio.Size = new System.Drawing.Size(172, 32);
            this.btnDesaparecio.TabIndex = 8;
            this.btnDesaparecio.Text = "Desaparecio";
            this.btnDesaparecio.UseVisualStyleBackColor = true;
            this.btnDesaparecio.Click += new System.EventHandler(this.btnDesaparecio_Click);
            // 
            // btnAparecio
            // 
            this.btnAparecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAparecio.Location = new System.Drawing.Point(28, 209);
            this.btnAparecio.Name = "btnAparecio";
            this.btnAparecio.Size = new System.Drawing.Size(171, 33);
            this.btnAparecio.TabIndex = 9;
            this.btnAparecio.Text = "Aparecerlo";
            this.btnAparecio.UseVisualStyleBackColor = true;
            this.btnAparecio.Click += new System.EventHandler(this.btnAparecio_Click);
            // 
            // btnLimpiarTxt
            // 
            this.btnLimpiarTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiarTxt.Location = new System.Drawing.Point(334, 357);
            this.btnLimpiarTxt.Name = "btnLimpiarTxt";
            this.btnLimpiarTxt.Size = new System.Drawing.Size(132, 37);
            this.btnLimpiarTxt.TabIndex = 10;
            this.btnLimpiarTxt.Text = "Limpiar Texto";
            this.btnLimpiarTxt.UseVisualStyleBackColor = true;
            this.btnLimpiarTxt.Click += new System.EventHandler(this.btnLimpiarTxt_Click);
            // 
            // btnEnviarTxt
            // 
            this.btnEnviarTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarTxt.Location = new System.Drawing.Point(334, 310);
            this.btnEnviarTxt.Name = "btnEnviarTxt";
            this.btnEnviarTxt.Size = new System.Drawing.Size(132, 33);
            this.btnEnviarTxt.TabIndex = 11;
            this.btnEnviarTxt.Text = "Enviarle Texto";
            this.btnEnviarTxt.UseVisualStyleBackColor = true;
            this.btnEnviarTxt.Click += new System.EventHandler(this.btnEnviarTxt_Click);
            // 
            // btnCamColLet
            // 
            this.btnCamColLet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCamColLet.Location = new System.Drawing.Point(27, 310);
            this.btnCamColLet.Name = "btnCamColLet";
            this.btnCamColLet.Size = new System.Drawing.Size(172, 33);
            this.btnCamColLet.TabIndex = 12;
            this.btnCamColLet.Text = "Cambiar el color de letra";
            this.btnCamColLet.UseVisualStyleBackColor = true;
            this.btnCamColLet.Click += new System.EventHandler(this.btnCamColLet_Click);
            // 
            // btnDesbloquear
            // 
            this.btnDesbloquear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesbloquear.Location = new System.Drawing.Point(334, 209);
            this.btnDesbloquear.Name = "btnDesbloquear";
            this.btnDesbloquear.Size = new System.Drawing.Size(132, 33);
            this.btnDesbloquear.TabIndex = 13;
            this.btnDesbloquear.Text = "Desbloquear";
            this.btnDesbloquear.UseVisualStyleBackColor = true;
            this.btnDesbloquear.Click += new System.EventHandler(this.btnDesbloquear_Click);
            // 
            // btnBloquear
            // 
            this.btnBloquear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBloquear.Location = new System.Drawing.Point(334, 163);
            this.btnBloquear.Name = "btnBloquear";
            this.btnBloquear.Size = new System.Drawing.Size(132, 32);
            this.btnBloquear.TabIndex = 14;
            this.btnBloquear.Text = "Bloquear Control";
            this.btnBloquear.UseVisualStyleBackColor = true;
            this.btnBloquear.Click += new System.EventHandler(this.btnBloquear_Click);
            // 
            // btnCamColFon
            // 
            this.btnCamColFon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCamColFon.Location = new System.Drawing.Point(27, 357);
            this.btnCamColFon.Name = "btnCamColFon";
            this.btnCamColFon.Size = new System.Drawing.Size(172, 37);
            this.btnCamColFon.TabIndex = 15;
            this.btnCamColFon.Text = "Cambiar el color de fondo";
            this.btnCamColFon.UseVisualStyleBackColor = true;
            this.btnCamColFon.Click += new System.EventHandler(this.btnCamColFon_Click);
            // 
            // txtTexto
            // 
            this.txtTexto.Location = new System.Drawing.Point(334, 280);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.Size = new System.Drawing.Size(132, 20);
            this.txtTexto.TabIndex = 16;
            // 
            // txtPanel
            // 
            this.txtPanel.Location = new System.Drawing.Point(27, 49);
            this.txtPanel.Multiline = true;
            this.txtPanel.Name = "txtPanel";
            this.txtPanel.Size = new System.Drawing.Size(438, 69);
            this.txtPanel.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 421);
            this.Controls.Add(this.txtPanel);
            this.Controls.Add(this.txtTexto);
            this.Controls.Add(this.btnCamColFon);
            this.Controls.Add(this.btnBloquear);
            this.Controls.Add(this.btnDesbloquear);
            this.Controls.Add(this.btnCamColLet);
            this.Controls.Add(this.btnEnviarTxt);
            this.Controls.Add(this.btnLimpiarTxt);
            this.Controls.Add(this.btnAparecio);
            this.Controls.Add(this.btnDesaparecio);
            this.Controls.Add(this.cbxFueCol);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Propiedades de control";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox cbxFueCol;
        private System.Windows.Forms.Button btnDesaparecio;
        private System.Windows.Forms.Button btnAparecio;
        private System.Windows.Forms.Button btnLimpiarTxt;
        private System.Windows.Forms.Button btnEnviarTxt;
        private System.Windows.Forms.Button btnCamColLet;
        private System.Windows.Forms.Button btnDesbloquear;
        private System.Windows.Forms.Button btnBloquear;
        private System.Windows.Forms.Button btnCamColFon;
        private System.Windows.Forms.TextBox txtTexto;
        private System.Windows.Forms.TextBox txtPanel;
    }
}

