﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnDesaparecio_Click(object sender, EventArgs e)
        {
            txtPanel.Visible = false;
        }

        private void btnAparecio_Click(object sender, EventArgs e)
        {
            txtPanel.Visible = true;
        }

        private void btnBloquear_Click(object sender, EventArgs e)
        {
            txtPanel.Enabled = false;
        }

        private void btnDesbloquear_Click(object sender, EventArgs e)
        {
            txtPanel.Enabled = true;
        }

        private void btnEnviarTxt_Click(object sender, EventArgs e)
        {
            txtPanel.Text = txtTexto.Text.Trim();
            txtTexto.ResetText();
        }

        private void btnLimpiarTxt_Click(object sender, EventArgs e)
        {
            txtPanel.ResetText();
            txtTexto.ResetText();
        }

        private void btnCamColLet_Click(object sender, EventArgs e)
        {
            if (cbxFueCol.SelectedIndex == 0)
            {
                txtPanel.ForeColor = Color.Blue;               
            }
            if (cbxFueCol.SelectedIndex == 1)
            {
                txtPanel.ForeColor = Color.Green;
            }
            if (cbxFueCol.SelectedIndex == 2)
            {
                txtPanel.ForeColor = Color.Red;
            }
            if (cbxFueCol.SelectedIndex == 3)
            {
                txtPanel.ForeColor = Color.Orange;
            }
            if (cbxFueCol.SelectedIndex == 4)
            {
                txtPanel.ForeColor = Color.Black;
            }
            if (cbxFueCol.SelectedIndex == 5)
            {
                txtPanel.ForeColor = Color.White;
            }
        }

        private void btnCamColFon_Click(object sender, EventArgs e)
        {
            if (cbxFueCol.SelectedIndex == 0)
            {
                txtPanel.BackColor = Color.Blue;
            }
            if (cbxFueCol.SelectedIndex == 1)
            {
                txtPanel.BackColor = Color.Green;
            }
            if (cbxFueCol.SelectedIndex == 2)
            {
                txtPanel.BackColor = Color.Red; 
            }
            if (cbxFueCol.SelectedIndex == 3)
            {
                txtPanel.BackColor = Color.Orange;
            }
            if (cbxFueCol.SelectedIndex == 4)
            {
                txtPanel.BackColor = Color.Black;
            }
            if (cbxFueCol.SelectedIndex == 5)
            {
                txtPanel.BackColor = Color.White;
            }
        }
    }
}
