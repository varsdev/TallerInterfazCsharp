﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form frm = new Multiplos3();
            Hide();
            frm.ShowDialog();
            Show();

        }

        private void numerosArregloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = new Arreglo();
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void nPrimoONoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = new Primo();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
