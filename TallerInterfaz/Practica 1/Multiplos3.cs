﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_1
{
    public partial class Multiplos3 : Form
    {
        public Multiplos3()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {

            int x, N = 100;
            listBox1.Items.Clear();
            for (x = 1; x <= N; x++)
            {
                if (x % 3 == 0)
                {
                    listBox1.Items.Add("Numero: " + x + " es multiplo de 3");
                }

            }

        }
    }
}
