﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_1
{
    public partial class Arreglo : Form
    {
        public Arreglo()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {

            int x, N = 20;
            List<int> ns = new List<int>();
            listBox1.Items.Clear();
            listBox2.Items.Clear();

            for (x = 1; x <= N; x++)
            {
                ns.Add(x);

            }
            foreach (int item in ns)
            {
                if (item % 2 == 0)
                {
                    listBox1.Items.Add("Numero: " + item);
                }
                else
                {
                    listBox2.Items.Add("Numero: " + item);

                }
            }


        }
    }
}
