﻿namespace Practica_1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.multiplos3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.numerosArregloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nPrimoONoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multiplos3ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(205, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // multiplos3ToolStripMenuItem
            // 
            this.multiplos3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.numerosArregloToolStripMenuItem,
            this.nPrimoONoToolStripMenuItem});
            this.multiplos3ToolStripMenuItem.Name = "multiplos3ToolStripMenuItem";
            this.multiplos3ToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.multiplos3ToolStripMenuItem.Text = "Ejercicios";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "Multiplos de 3";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // numerosArregloToolStripMenuItem
            // 
            this.numerosArregloToolStripMenuItem.Name = "numerosArregloToolStripMenuItem";
            this.numerosArregloToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.numerosArregloToolStripMenuItem.Text = "Numeros Arreglo";
            this.numerosArregloToolStripMenuItem.Click += new System.EventHandler(this.numerosArregloToolStripMenuItem_Click);
            // 
            // nPrimoONoToolStripMenuItem
            // 
            this.nPrimoONoToolStripMenuItem.Name = "nPrimoONoToolStripMenuItem";
            this.nPrimoONoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nPrimoONoToolStripMenuItem.Text = "N primo o no";
            this.nPrimoONoToolStripMenuItem.Click += new System.EventHandler(this.nPrimoONoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(205, 159);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Practica 1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem multiplos3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem numerosArregloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nPrimoONoToolStripMenuItem;
    }
}

