﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_1
{
    public partial class Primo : Form
    {
        public Primo()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            int num = int.Parse(txtN.Text.Trim());
            int a = 0;

            for (int i = 1; i < (num + 1); i++)
            {
                if (num % i == 0)
                {
                    a++;
                }
            }
            if (a != 2)
            {
                MessageBox.Show(num + " No es primo");
            }
            else
            {
                MessageBox.Show(num + " Si es primo");
            }


        }
    }
}
